from pprint import pprint
from peerplays import PeerPlays
from peerplaysbase import operations

ppy = PeerPlays()

# put your password here
ppy.wallet.unlock("password")

amount_to_bet = {
    "asset_id": "1.3.0",
    "amount": 100 * 100000 # core asset has precision 5
}

op_params = {
    "fee": {
        "asset_id": "1.3.0",
        "amount": 0
    },
    "bettor_id": "1.2.25", # this is toto23 account id
    "betting_market_id": "1.21.2",
    "amount_to_bet": amount_to_bet,
    "backer_multiplier": 120 * 10000, # betting odds has precision 4
    "amount_reserved_for_fees": amount_to_bet["amount"] * 0.02, # default fee is 2%
    "back_or_lay": "back",
    "extensions": {}
}

# create ops
op = operations.Bet_place(**op_params)
pprint(ppy.finalizeOp(op, "toto23", "active"))
