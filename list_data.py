from pprint import pprint
from peerplays import PeerPlays
from peerplays.proposal import Proposal, Proposals
from peerplays.account import Account
from peerplays.sport import Sports, Sport
from peerplays.witness import Witnesses, Witness
from peerplays.eventgroup import EventGroups, EventGroup
from peerplays.bettingmarketgroup import BettingMarketGroup, BettingMarketGroups
from peerplays.bettingmarket import BettingMarket, BettingMarkets

ppy = PeerPlays()

# put your password here
ppy.wallet.unlock("password")

# pprint(EventGroups("1.16.4"))
pprint(BettingMarketGroups("1.18.0"))
# pprint(BettingMarketGroup("1.20.76").items())
# pprint(ppy.rpc.get_objects(["2.0.0"]))
# pprint(BettingMarket("1.21.64").items())
# pprint(BettingMarkets("1.20.32"))
# pprint(Proposals("witness-account"))
# pprint(Account("toto02"))
# pprint(Account("witness-account"))
# for witness in Witnesses():
#     pprint(witness.items())
