import datetime
from pprint import pprint
from peerplays import PeerPlays
ppy = PeerPlays(
    # this account creates the proposal
    proposer="init0",
    # Proposal needs to be approve within 5 minutes
    proposal_expiration=60 * 30,
    # For testing, set this to true
    nobroadcast=False,
    # We want to bundle many operations into a single transaction
    bundle=True,
)

# put your password here
ppy.wallet.unlock("password")

ppy.sport_create([          # relative id 0.0.0
    ["en", "Ice Hockey"],
], account="witness-account")

ppy.event_group_create(
    [
        ["en", "AIHL"],
    ],
    sport_id="1.16.4",
    account="init0"
)


ppy.event_create(
    [
        ["en", "Perth Thunder vs Sidney Ice Dogs"]
    ],
    [
        ["en", "2017-2018"]
    ],  # season
    datetime.datetime(2017, 9, 1, 0, 0, 0),  # start_time
    event_group_id="1.17.9",   # event group
    account="init0"
)


ppy.betting_market_rules_create(
    [
        ["en", "AIHL Rules v1.3"]
    ],
    [
        ["en", "The winner will be the team with the most points at the end of the game.  The team with fewer points will not be the winner."]
    ],
)

ppy.betting_market_group_create(
    [
        ["en", "Moneyline",]
    ],
    event_id="0.0.0",
    rules_id="0.0.1",
    account="init0"
)


ppy.betting_market_create(
    payout_condition=[
        ["en", "Perth Thunder"]
    ],
    group_id="0.0.2",
    account="init0"
)

ppy.betting_market_create(
    payout_condition=[
        ["en", "Sidney Ice Dogs"]
    ],
    group_id="0.0.2",
    account="init0"
)

# Broadcast the whole transaction
pprint(
    ppy.txbuffer.broadcast()
)