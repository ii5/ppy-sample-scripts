import datetime
from pprint import pprint
from peerplays import PeerPlays
ppy = PeerPlays(
    # this account creates the proposal
    proposer="init0",
    # Proposal needs to be approve within 5 minutes
    proposal_expiration=60 * 30,
    # For testing, set this to true
    nobroadcast=False,
    # We want to bundle many operations into a single transaction
    bundle=True,
)

# put your password here
ppy.wallet.unlock("password")

ppy.betting_market_resolve(betting_market_group_id="1.20.32",
                           results=[
                               ["1.21.64", "win"],
                               ["1.21.65", "win"]
                           ],
                           account="witness-account")


# Broadcast the whole transaction
pprint(
    ppy.txbuffer.broadcast()
)