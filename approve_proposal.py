from pprint import pprint
from peerplays import PeerPlays
from peerplays.proposal import Proposals

ppy = PeerPlays(
    bundle=True,
    nobroadcast=False
)

# put your password here
ppy.wallet.unlock("password")

proposal_id = "1.10.40"

# 6 accounts are enough to approve the proposal
ppy.approveproposal([proposal_id], "init0")
ppy.approveproposal([proposal_id], "init1")
ppy.approveproposal([proposal_id], "init2")
ppy.approveproposal([proposal_id], "init3")
ppy.approveproposal([proposal_id], "init4")
ppy.approveproposal([proposal_id], "init5")
# ppy.approveproposal([proposal_id], "init6")
# ppy.approveproposal([proposal_id], "init7")
# ppy.approveproposal([proposal_id], "init8")
# ppy.approveproposal([proposal_id], "init9")
# ppy.approveproposal([proposal_id], "init10")

pprint(
    ppy.txbuffer.broadcast()
)